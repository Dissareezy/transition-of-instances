﻿# Вы можете расположить сценарий своей игры в этом файле.

# Определение персонажей игры.
define d = Character('Damian', color="#c8ffc8")
define unkown = Character('???', color="c8ffc8")
# Вместо использования оператора image можете просто
# складывать все ваши файлы изображений в папку images.
# Например, сцену bg room можно вызвать файлом "bg room.png",
# а eileen happy — "eileen happy.webp", и тогда они появятся в игре.

# Игра начинается здесь:
label start:

    scene bg roomnight

    show damian thinking

    unkown "Черт... Опять эти кошмары"

    show damian skeptical

    d "Сейчас ночь или утро?.."

    show damian thougtfullysmiles

    d "Ох, опять эта комната"


    return
